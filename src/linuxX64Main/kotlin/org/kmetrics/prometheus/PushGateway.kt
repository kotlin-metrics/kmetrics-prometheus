package org.kmetrics.prometheus

import curl.*
import kotlinx.cinterop.*
import org.kmetrics.core.Metric

/** Covers Prometheus Push Gateway functionality. */
actual class PushGateway(
    actual val host: String = "localhost",
    actual val port: Int = 9091,
    @Suppress("CanBeParameter") actual val timeout: Int = 10_000
) {
    private val globalArena = Arena()
    private val headerList = globalArena.alloc<curl_slist>()
    private var _isClosed = false
    actual val isClosed: Boolean
        get() = _isClosed

    private fun setupHeaders(curlHandle: COpaquePointer?) {
        curl_slist_append(headerList.ptr, "Content-Type: text/plain")
        // Set the custom HTTP request headers.
        curl_easy_setopt(curlHandle, CURLOPT_HTTPHEADER, headerList.next)
    }

    /**
     * Pushes a single metric to the Push Gateway.
     * @param job - Name of the job.
     * @param metric - The metric to push.
     */
    actual suspend fun <T : Number> pushMetric(job: String, metric: Metric<T>) {
        if (!isClosed) {
            val curlHandle = curl_easy_init()
            setupHttpRequest(curlHandle = curlHandle, metric = metric, job = job)
            // Do the HTTP request.
            curl_easy_perform(curlHandle)
            curl_easy_cleanup(curlHandle)
        }
    }

    private fun <T : Number> setupHttpRequest(curlHandle: COpaquePointer?, metric: Metric<T>, job: String) {
        // Set the HTTP request method.
        curl_easy_setopt(curlHandle, CURLOPT_CUSTOMREQUEST, "PUT")
        // Set the URL.
        curl_easy_setopt(curlHandle, CURLOPT_URL, "http://$host:$port/metrics/job/$job")
        // Set the HTTP request body.
        curl_easy_setopt(curlHandle, CURLOPT_COPYPOSTFIELDS, metric.createText())
        // Set the HTTP timeout in milli seconds.
        curl_easy_setopt(curlHandle, CURLOPT_TIMEOUT_MS, timeout)
        setupHeaders(curlHandle)
    }

    actual suspend fun <T : Number> pushAllMetrics(job: String, vararg metrics: Metric<T>) {
        if (!isClosed) {
            metrics.forEach { m ->
                val curlHandle = curl_easy_init()
                setupHttpRequest(curlHandle = curlHandle, metric = m, job = job)
                curl_easy_perform(curlHandle)
                curl_easy_cleanup(curlHandle)
            }
        }
    }

    actual fun close() {
        if (!isClosed) {
            // TODO: Figure out why headerList's pointer isn't being freed.
//            curl_slist_free_all(headerList.ptr)
            globalArena.clear()
            _isClosed = true
        }
    }

    actual fun cancelPushes() {
        // TODO: Cancel all HTTP operations.
    }
}
