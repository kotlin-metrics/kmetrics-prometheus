package org.kmetrics.prometheus

import org.kmetrics.core.Metric

/** Covers Prometheus Push Gateway functionality. */
expect class PushGateway {
    val host: String
    val port: Int
    val isClosed: Boolean
    val timeout: Int

    /**
     * Pushes a single metric to the Push Gateway.
     * @param job - Name of the job.
     * @param metric - The metric to push.
     */
    suspend fun <T : Number> pushMetric(job: String, metric: Metric<T>)

    /**
     * Pushes all metrics to the Push Gateway in parallel.
     * @param job - Name of the job.
     * @param metrics - The metrics to push.
     */
    suspend fun <T : Number> pushAllMetrics(job: String, vararg metrics: Metric<T>)

    fun close()

    fun cancelPushes()
}
